import React, { Component } from 'react';
import Strawpolls from './Components/Strawpolls';
import AddStrawpoll from './Components/AddStrawpoll';
//import uuid from 'uuid';
import './App.css';

class App extends Component {
  constructor(){
    super();
    this.state = {}
  }

  componentWillMount(){
    this.updateStrawpollList();
  }

  handleAddStrawpoll(strawpoll){
      fetch("http://localhost:8080/api/v1/strawpolls", {
          method: "POST",
          headers: {
              "Content-type": "application/json",
          },
          body: JSON.stringify({
              "title": strawpoll.title,
              "questions": strawpoll.questions
          })
      });

      this.updateStrawpollList();
  }

  handleDeleteStrawpoll(id){
      fetch("http://localhost:8080/api/v1/strawpolls/" + id, {
          method: "DELETE",
          headers: {
              Accept: "application/json",
              "Content-type": "application/json",
          }
      }).then(() => this.updateStrawpollList());

  }

  updateStrawpollList(){
      fetch("http://localhost:8080/api/v1/strawpolls")
        .then(result => result.json())
        .then(strawpolls=>{
            this.setState({strawpolls});
          }
        )
  }

  render() {
    return (
      <div className="App">
        <AddStrawpoll
            addStrawpoll={this.handleAddStrawpoll.bind(this)}
        />
        <Strawpolls
            strawpolls={this.state.strawpolls}
            onDelete={this.handleDeleteStrawpoll.bind(this)}
        />
      </div>
    );
  }
}

export default App;
