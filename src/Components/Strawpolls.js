import React, { Component } from 'react';
import StrawpollItem from './StrawpollItem';

class Strawpolls extends Component {
    deleteStrawpoll(id){
        this.props.onDelete(id);
    }

  render() {
    let strawpollItems;
    if(this.props.strawpolls){
      strawpollItems = this.props.strawpolls.map(strawpoll => {
        //console.log(strawpoll);
        return(
          <StrawpollItem onDelete={this.deleteStrawpoll.bind(this)} key={strawpoll.id} strawpoll={strawpoll} />
        );
      });
    }

    return (
      <div className="Strawpolls">
        <h3>Latest strawpolls</h3>
        {strawpollItems}
      </div>
    );
  }
}

Strawpolls.propTypes = {
    strawpolls: React.PropTypes.array
}

export default Strawpolls;
