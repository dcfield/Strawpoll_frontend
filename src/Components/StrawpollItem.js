import React, { Component } from 'react';

class StrawpollItem extends Component {
    deleteStrawpoll(id){
        this.props.onDelete(id);
    }

    render() {
        return (
          <li className="Strawpoll">
            <strong>{this.props.strawpoll.title} - {this.props.strawpoll.questions}</strong><a href="#" onClick={this.deleteStrawpoll.bind(this, this.props.strawpoll.id)}>X</a>
          </li>
        );
    }
}

export default StrawpollItem;
