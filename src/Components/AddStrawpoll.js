import React, { Component } from 'react';

class AddStrawpoll extends Component {
    constructor(){
        super();
        this.state={
            newStrawpoll: {
                title: '',
                questions: []
            },
            questionInputs: []
        }
    }

    // Handle form submit
    handleSubmit(e){
        e.preventDefault();
        if(this.refs.title.value === ''){
            alert('Title is required');
        }else{
            //console.log(this.refs.questions.value);
            this.setState({
                newStrawpoll: {
                    title: this.refs.title.value,
                    questions: this.state.newStrawpoll.questions
                }
            }, function(){
                this.props.addStrawpoll(this.state.newStrawpoll);
            });
        }
    }

    // Add a new question field
    appendInput(){
        this.setState(prevState => ({ questionInputs: [...prevState.questionInputs, '']}))
    }

    // Update strawpoll state when new question is typed
    handleChange(i, event){
        let questions = [...this.state.newStrawpoll.questions];
        questions[i] = event.target.value;
        let newStrawpoll = this.state.newStrawpoll;
        newStrawpoll.questions = questions;
        this.setState({newStrawpoll});
    }

    render() {
        return (
          <div>
            <h3>Add strawpoll</h3>
            <form onSubmit={this.handleSubmit.bind(this)}>
              <div>
                <label>Title</label>
                <input type="text" ref="title" />
              </div>
              <div className="questions">
                {this.state.questionInputs.map(
                    (input, i) => (
                        <div key={i}>
                            <label>Question</label>
                            <input type="text" ref="questions" onChange={this.handleChange.bind(this, i)}/>
                        </div>
                    )
                )}
              </div>

              <input type="submit" value="Submit" />
            </form>
            <div id="addQuestion">
              <button onClick={this.appendInput.bind(this)}>Add another question</button>
            </div>
          </div>
        );
    }
}

export default AddStrawpoll;
